#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

local $\ = "\n";
local $, = "\t";

use Data::Dumper;
use File::Spec::Functions qw/catfile/;
use List::Util q/first/;

my $cls = shift;
my $blasts = {
    custom => shift,
    dna => shift
};

my $fh;


my $totals = {};
my $samples = {};
my $comparative = [];
my $reads = {};

open($fh, '<', $cls);
while(<$fh>){
    next unless s/^>CL//;
    chomp;
    my ($cluster, $total) = split;

    my $data = {custom => {}, dna => {}, reads => {}, total => $total, name => $cluster};

    foreach my $read (split "\t", readline($fh)){
        my $sample = substr($read, 0, rindex($read, '_'));
        $samples->{$sample}++;

        $data->{reads}->{$sample} ++;

        $reads->{$read} = $data;
    }

    push(@$comparative, $data);
}
close($fh);

foreach my $db (qw/ custom dna /){
    open $fh, $blasts->{$db};
    while (<$fh>){
        $_ = [split /\t/];
        $_->[1] =~ s/.*#//;

        my $data = $reads->{$_->[0]};
        next unless $data;

        $data->{$db}->{$_->[1]} += 1/$data->{total};
    }
    close $fh;
}


my $header = [sort keys $samples];
print "# Total counts:", "", @$samples{@$header};
print "#";
print '"cluster"',  map {"\"$_\""} @$header;
foreach my $data (@$comparative){

    print $data->{name}, annotate($data), map {$data->{reads}->{$_} || 0 } @$header;
}



sub annotate {
    my $data = shift;
    my $dna = $data->{dna};


    if(keys %$dna){
        #Compine all rDNA elements
        my $rDNAv = 0;
        foreach my $rDNAk (grep {/rDNA/} keys %$dna){
            $rDNAv += $dna->{$rDNAk};
            delete $dna->{$rDNAk};
        }
        $dna->{'rDNA'} = $rDNAv;

        ## get the largest organelle mapping to at least 40% of reads
        my $organelle = first { $dna->{$_} >= 0.4 } sort {$dna->{$b} <=> $dna->{$a}} keys %$dna;
        if($organelle){
            return $organelle;
        }
    }
    ## Get percent of hits in custom database (repeats)
    my $repeats = $data->{custom};

    # Remove 'Unknown' elements
    delete $repeats->{Unknown};

    if(keys %$repeats){
        # Combine 'gypsy' and 'LTR/Gypsy' elements
        if($repeats->{gypsy}){
            $repeats->{'LTR/Gypsy'} += $repeats->{gypsy};
            delete $repeats->{'gypsy'};
        }


        # Sort elements descending by count
        my $sort_header = [sort {$repeats->{$b} <=> $repeats->{$a}} keys %$repeats];

        # Check if first element is 'LTR'
        if($sort_header->[0] eq 'LTR'){
            # If second element is an LTR and third element is not LTR or is less
            # than half the second, add the first and second elements together and
            # remove the LTR element
            if ($sort_header->[1] && $sort_header->[1] =~ /^LTR/){
                if(!$sort_header->[2] || $sort_header->[2] !~ /^LTR/ ||
                   $repeats->{$sort_header->[1]}/2 > $repeats->{$sort_header->[2]}){

                    $repeats->{$sort_header->[1]} += $repeats->{LTR};
                    delete $repeats->{LTR};
                }
                # If second element is non-LTR and is larger than half the LTR,
                # classify cluster as Retroelement
            }elsif($sort_header->[1] && $sort_header->[1] eq 'non-LTR_retroposon' &&
                   $repeats->{$sort_header->[0]}/2 < $repeats->{$sort_header->[1]}){
                return 'Retroelement';
            }
        }else{
            # Remove LTR if not first element
            delete $repeats->{LTR};
        }
        # Resort elements
        $sort_header = [sort {$repeats->{$b} <=> $repeats->{$a}} keys %$repeats];

        # IF the top two elements are LTR, they're close in size, and are larger
        # than 10% of the cluster, classify cluster as 'LTR'
        if(@$sort_header >= 2 && $sort_header->[0] =~ /^LTR/ && $sort_header->[1] =~ /^LTR/ &&
           $repeats->{$sort_header->[0]}/2 < $repeats->{$sort_header->[1]} &&
           ($repeats->{$sort_header->[0]}+$repeats->{$sort_header->[1]}) > 0.10){
            return 'LTR';
        }

        # If the first element is 'DNA' and the second element is a DNA element,
        # add the elements together and delete the DNA element
        if($sort_header->[0] eq 'DNA'){
            if ($sort_header->[1] && $sort_header->[1] =~ /^DNA/){
                $repeats->{$sort_header->[1]} += $repeats->{DNA};
                delete $repeats->{DNA};
            }
        }else{
            # Remove DNA if not first element
            delete $repeats->{DNA};
        }
        # Resort elements
        $sort_header = [sort {$repeats->{$b} <=> $repeats->{$a}} keys %$repeats];


        # If top element is larger than 10% total count and is 1.5x larger than
        # second element, classify cluster as the top element; else leave the
        # cluster unclassified (*).
        if($sort_header->[0] && $repeats->{$sort_header->[0]} > 0.10 &&
           $repeats->{$sort_header->[0]} > (($sort_header->[1] && $repeats->{$sort_header->[1]} || 0) * 1.5)){
            return $sort_header->[0];
        }else{
            return  '*';
        }
    }
    return 'NA';
}
