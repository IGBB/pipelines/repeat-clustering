#!/usr/bin/env bash


SRA=$1;

URL=ftp://ftp.sra.ebi.ac.uk/vol1/fastq/${SRA:0:6}/

if [ ${#SRA} -gt 9 ]; then
    DIR=00${SRA:9}
    URL+=${DIR: -3}/
fi

URL+=$SRA

wget -O $2 $URL/${SRA}_1.fastq.gz || wget -O $2 $URL/${SRA}.fastq.gz
