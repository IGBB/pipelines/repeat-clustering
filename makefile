##
# Project Title
#
# @file
# @version 0.1

.SECONDARY_EXPANSION:

# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(pipeline)/config.mk


#########################################
####### Required Input Parameters #######
#########################################
#                    NAME      Description
$(call var_required, CPUS,     Number of threads to use for each task)
$(call var_required, DATABASE, Custom Repeat Database)

.PHONY: all
all: download cutadapt sample repex annotate

################################################################################
## 0) Download Data
################################################################################
.PHONY: download

SRA_FILES := $(addprefix 0-raw/, $(addsuffix .fq.gz, $(SRA)))
# TODO: Add option to include data not on SRA
ALL = $(SRA) $(LOCAL)
ALL_FILES = $(SRA_FILES) $(foreach lib,$(LOCAL),$($(lib)))

.SECONDARY:$(ALL_FILES)

echo:
	echo $(ALL)
	echo $(ALL_FILES)


download: $(SRA_FILES)

$(SRA_FILES) : 0-raw/%.fq.gz: | 0-raw/
	$(pipeline)/download.sh $* $@.tmp 2>$@.log
	mv $@.tmp $@

################################################################################
##  1) Adapter Trimming (cutadapt)
# Read trimming and filtering using cutadapt with following criteria:
#  - trim Illumina TruSeq Universal Primer
#  - quality trim any base quality score less than 15
#  - discard any read with Ns
#  - discard reads shorter than 90 bases
#  - cut reads longer than 90 bases
################################################################################
.PHONY: cutadapt

TRIM_FILES := $(addprefix 1-trim/, $(addsuffix .fq.gz, $(ALL)))

.SECONDARY: $(TRIM_FILES)
cutadapt: $(TRIM_FILES)

$(TRIM_FILES): 1-trim/% : 0-raw/% | 1-trim/
	ml singularity cutadapt && \
	cutadapt -j $(CPUS) \
		-a "AGATCGGAAGAGCACACGTCTGAACTCCAGTCA" \
		--quality-cutoff 15 \
		--max-n 0  \
		--minimum-length 90 \
		--length 90 \
		-o $@.tmp  \
		$< > $@.log
	mv $@.tmp $@


################################################################################
##  2) Sample
################################################################################
.PHONY: presample

PRESAMP_FILES := $(addprefix 2-sample/, $(addsuffix .fa, $(ALL)))

.SECONDARY: $(PRESAMP_FILES)
presample: $(PRESAMP_FILES)

$(PRESAMP_FILES) : 2-sample/%.fa : 1-trim/%.fq.gz | 2-sample/
	$(pipeline)/reservoir_sample/reservoir_sample -n "$*" -l 90 -s $$($($*_SIZE) * 2 ) $< > $@.tmp
	mv $@.tmp $@



################################################################################
##  3) Filter
################################################################################
.PHONY: filter

FILT_BLAST := $(addprefix 3-filter/, $(addsuffix .blast, $(ALL)))
FILT_LIST := $(addprefix 3-filter/, $(addsuffix .list, $(ALL)))
FILT_FILES := $(addprefix 3-filter/, $(addsuffix .fa, $(ALL)))

filter: $(FILT_FILES)

$(FILT_BLAST) : 3-filter/%.blast : 2-sample/%.fa | 3-filter/
	$(call sexec,$^,$(repex)) blastn \
		-db /opt/repex_tarean/databases/dna_database_masked.fasta \
		-query $< \
		-out $@.tmp \
		-num_threads $(CPUS) \
		-outfmt 6
	mv $@.tmp $@

$(FILT_LIST) : 3-filter/%.list : 2-sample/%.fa 3-filter/%.blast | 3-filter/
	sed 's/>//p' -n  $< | grep -x -F -f <(cut -f 1 $(word 2,$^) | uniq) -v >$@.tmp
	mv $@.tmp $@

$(FILT_FILES) : 3-filter/%.fa : 3-filter/%.list 2-sample/%.fa | 3-filter/
	$(call sexec,$^,$(samtools)) samtools faidx -r $^ > $@.tmp
	mv $@.tmp $@


################################################################################
##  4) Sample
################################################################################
.PHONY: sample

SAMP_FILES := $(addprefix 4-sample/, $(addsuffix .fa, $(ALL)))

.SECONDARY: $(SAMP_FILES)
sample: $(SAMP_FILES)

$(SAMP_FILES) : 4-sample/%.fa : 3-filter/%.fa | 4-sample/
	$(pipeline)/reservoir_sample/reservoir_sample -n "$*" -l 90 -s $($*_SIZE) $< > $@.tmp
	mv $@.tmp $@


################################################################################
##  3) Repeat Explorer
################################################################################
.PHONY: repex

repex: 3-repex/complete

3-repex/combine.fa: $(SAMP_FILES) | 3-repex/
	cat $^ > $@.tmp
	mv $@.tmp $@

3-repex/complete: 3-repex/combine.fa $(DATABASE) | 3-repex/
	$(call sexec,$^,$(repex)) seqclust \
		-tax VIRIDIPLANTAE3.0 \
		-opt ILLUMINA \
		-d $(DATABASE) custom  \
		-l repex.log \
		-c $(CPUS) \
		-P $(NAME_LENGTH) \
		-v $| \
		--keep_names \
		$<
	touch $@

################################################################################
##  4) Annotate
################################################################################
.PHONY: annotate

annotate: clusters.annotate.txt

clusters.annotate.txt : 3-repex/complete
	perl $(pipeline)/annotate.pl \
		3-repex/seqclust/clustering/hitsort.cls \
		<(cat 3-repex/seqclust/blastn/custom_db_custom_reads.fasta.*) \
		<(cat 3-repex/seqclust/blastn/dna_database_reads.fasta.*) > $@.tmp
	mv $@.tmp $@

# end
